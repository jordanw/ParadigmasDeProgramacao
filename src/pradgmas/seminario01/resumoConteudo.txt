SEMNINÁRIO DE PARADIGMAS DE PROGRAMAÇÃO

PADRÃO DE PROJETO COMPOSITE

INTRODUÇÃO
A ideia desse padrão é montar uma árvore onde tanto as folhas (objetos individuais) 
quanto os compostos (grupos de objetos) sejam tratados de maneira igual. Em termos de 
orientação a objetos, isso significa aplicarmos polimorfismo para chamar métodos de um
objeto na árvore sem nos preocuparmos se ele é uma folha ou um composto.

Ele (Composite) pode ser empregado, como dito em sua intenção, sempre que quisermos tratar 
de maneira igual objetos individuais e composiçõres de objetos.

********************************************************************************************************************************

Você pode tratar toda a coleção de objetos da mesma maneira que trataria qualquer um dos objetos individuais na coleção. 
Funções executadas no Composite são transmitidas a cada um dos filhos a serem executados. 
Em grandes coleções, isso se torna muito benéfico 
(embora também possa ser enganoso, pois você pode não perceber o quão grande é a coleta e, então, não entender o quanto o desempenho pode sofrer).

Ele organiza os objetos em uma estrutura de árvore e, uma vez que cada objeto composite contém um método para obter seus filhos, 
você pode ocultar a implementação e organizar os filhos em qualquer forma que desejar.

INTENÇÃO:
“Compor objetos em estruturas de árvore para representarem hierarquias parte-todo. Composite 
permite aos clientes tratarem de maneira uniforme objetos individuais e composições de objetos.” 
(Design Patterns: Elements of Reusable Object-Oriented Software)

*********************************************************************************************************************************
O padrão Composite é utilizado quando queremos reutilizar método e funções de algum objeto e colocar em qualquer outro, 
sendo vários objetos ligados a um único.

**********************************************************************************************************************************
Com o uso do padrão Composite podemos criar objetos com uma grande complexidade e eles serem compostos por outros objetos menores, 
além de deixar o código bem estruturado e de fácil entendimento, sendo rápida a forma de adicionar novos componentes, métodos e 
funções.

**********************************************************************************************************************************
Compor objetos em estruturas em árvore para representar hierarquias Parte-Todo
Composite permite que clientes tratem objetos individuais e composições uniformemente

Motivação:
Em certas estruturas necessitamos por muitas vezes de métodos e funções que já estão especificadas em um determinado objeto, 
sendo necessário apenas a ligação entre os dois para assim poder ser reutilizado o código.

Aplicabilidade:
 Este padrão pode ser usado sempre que desejarmos construir objetos exisitindo objetos do mesmo tipo.



ESTRUTURA DO COMPOSITE:

O Componente: é a interface que define métodos comuns às classes dentro da composição e, opcionalmente, 
define uma interface para acessar o objeto “pai” de um componente.

A Folha: é um componente que, como o nome indica, não possui filhos (está nas extremidades da árvore).

O Composto: é um componente que, como o nome indica, é composto de outros componentes, que podem ser folhas ou outros 
compostos.

*********************************************************************************************************************************

Componente:
* Declara a interface para objetos na  composição
* Implementa comportamento default para  interface comum a todas as classes, como apropriado; 
* Declara uma interface para acessar ou gerenciar seus 
* Componentes filhos;

Folha: 
*Representa objetos folhas na composição. Uma folha não tem filhos; 
*Define comportamento para objetos  primitivos na composição.

Composição: 
* Define comportamento para Componentes que têm filhos;
* Armazena Componentes filhos; 
* Implementa operações relacionadas com filhos na interface do Componente.
 Cliente: 
*Manipula objetos na composição através da interface Componente.

***********************************************************************************************************************************
Os nomes genéricos dados às classes abstratas são Component e Composite
Os nomes genéricos dados às classes concretas são Leaf e ConcreteComposite

***********************************************************************************************************************************

Adicionar referência ao pai de um objeto pode simplificar o caminhamento na estrutura
Onde adicionar a referência ao pai? Normalmente é colocada na classe abstrata Component
As subclasses herdam a referência e os métodos que a gerenciam

Compartilhamento de componentes
Útil para reduzir as necessidades de espaço
Fazer isso complica se os componentes só puderem ter um único pai


CONSEQUÊNCIA DO COMPOSITE:
Objetos complexos podem ser compostos de objetos mais simples recursivamente
Facilita a adição de novos componentes: o cliente não tem que mudar com a adição de novos objetos (simples ou compostos)
Do lado negativo, o projeto fica geral demais
É mais difícil restringir os componentes de um objeto composto
O sistema de tipagem da linguagem não ajuda a detectar composições erradas
A solução é verificar em tempo de execução


VANTAGENS:
Adicionar referência ao pai de um objeto pode simplificar o caminhamento na estrutura
As subclasses herdam a referência e os métodos que a gerenciam
Objetos complexos podem ser compostos de objetos mais simples recursivamente
Facilita a adição de novos componentes

DESVANTAGENS:
É mais difícil restringir os componentes de um objeto composto
O sistema de tipagem da linguagem não ajuda a detectar composições erradas
A solução é verificar em tempo de execução
