package paradigmas.exercicios.unidade2;


// POJO (plain old java object)
public class Musica {
	
	private String nome;
	private String artista;
	private String album;
	private String ano;
	
	public Musica(){
		
	}
	
	public Musica(String nome, String artista, String album, String ano) {
		super();
		this.nome = nome;
		this.artista = artista;
		this.album = album;
		this.ano = ano;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getArtista() {
		return artista;
	}

	public void setArtista(String artista) {
		this.artista = artista;
	}

	public String getAlbum() {
		return album;
	}

	public void setAlbum(String album) {
		this.album = album;
	}

	public String getAno() {
		return ano;
	}

	public void setAno(String ano) {
		this.ano = ano;
	}
	
	
	
	
	
}
