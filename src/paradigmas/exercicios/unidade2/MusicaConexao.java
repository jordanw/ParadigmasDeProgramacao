package paradigmas.exercicios.unidade2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MusicaConexao 
{
	
	private static final String DRIVER = "com.mysql.jdbc.Driver";
	private static final String URL = "jdbc:mysql://localhost:3306/paradigmas";
	private static final String USER = "root";
	private static final String PASS = "jordanw";
	
	public static Connection getConnection()
	{
		try 
		{
			//carrega o driver especificado
			Class.forName("com.mysql.jdbc.Driver");
			//abre a conexao
			return DriverManager.getConnection(URL, USER, PASS);
			/* sobrecargas de getConnection
			 * 
			 * public static Connection getConnection(String url)
			   public static Connection getConnection(String url, Properties info)
               public static Connection getConnection(String url, String user, String password)
			 * */
		} 
		catch (ClassNotFoundException e) 
		{
			throw new RuntimeException("Erro na conexão hshhshs", e);
		} catch (SQLException e) 
		{
			throw new RuntimeException("Erro na conexão commmm", e);
		}			
	}
	
	public static void closeConnection(Connection con)
	{
		if(con != null){
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void closeConnection(Connection con, PreparedStatement stmt)
	{
		closeConnection(con);
		try
		{
			if(stmt != null)
			{
				stmt.close();
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
	
	public static void closeConnection(Connection con, PreparedStatement stmt, ResultSet rs)
	{
		closeConnection(con, stmt);
		try
		{
			if(rs != null)
			{
				rs.close();
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
	
	

}
