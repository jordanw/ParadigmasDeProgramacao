package paradigmas.exercicios.unidade2;

import java.lang.reflect.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.JOptionPane;

import com.mysql.jdbc.Statement;

//crud

public class MusicaDAO {
	
	List<Musica> listMusicas = new ArrayList<Musica>();
	List<Musica> recupera () {
		// consulta um banco um dados ou um servico web
		
		return Arrays.asList(
			new Musica ("Immortalized", "Disturbed", "Immortalized", "2015"),
			new Musica ("Imaginaerum", "Nightwish", "Imaginaerum", "2011"),
			new Musica ("Angels Don't Kill", "Children of Bodom", "HateCrew Deathroll", "2003"),
			new Musica ("Cry To The Moom ", "Epica", "The Phantom Agony", "2003")
		);		
	}
	
	public void create(Musica m)
	{
		Connection con = MusicaConexao.getConnection();
		PreparedStatement stmt = null;
		try{
			stmt = con.prepareStatement("INSERT INTO musicas VALUES (?,?,?,?)");
			stmt.setString(0, m.getNome());
			stmt.setString(1, m.getArtista());
			stmt.setString(2, m.getAlbum());
			stmt.setString(3, m.getAno());
			stmt.executeUpdate();
			JOptionPane.showMessageDialog(null, "Salvo com sucesso");
		}catch(SQLException e)
		{
			//e.printStackTrace();
			JOptionPane.showMessageDialog(null, "Arquivo não salvo: " + e);
		}finally{
			MusicaConexao.closeConnection(con, stmt);
		}		
	}
	
	public List<Musica> read()
	{
		Connection con = MusicaConexao.getConnection();
		
		/*usado para criar um objeto que representa a instrução SQL que será executada, 
		 * sendo que é invocado através do objeto Connetion.
		 */
		
		//PreparedStatement stmt = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			/*createStatement() usado para criar um objeto Statement 
			 * que executa instruções SQL ao banco de dados.
			 * */
			 
			stmt = (Statement) con.createStatement();
			//executa uma instrução SQL que retorna um único objeto ResultSet.
			rs = stmt.executeQuery("SELECT * FROM musicas");
			
			while(rs.next())
			{
				Musica musica = new Musica();
				musica.setNome(rs.getString("musica"));
				musica.setArtista(rs.getString("artista"));
				musica.setAlbum(rs.getString("album"));
				musica.setAno(rs.getString("ano"));
				listMusicas.add(musica);
				
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("ERROR DE SQL DAO");
		}finally
		{
			MusicaConexao.closeConnection(con);
		}
		
		return listMusicas;
		
	}

}
