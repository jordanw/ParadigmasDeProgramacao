package paradigmas.exercicios.unidade2;

import java.awt.FlowLayout;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

public class Teste {
	
	public static void main(String[] args) {

		MusicaDAO dao = new MusicaDAO();
		MusicaTableModel model = new MusicaTableModel(dao);
		JTable jt = new JTable (model);
		JFrame frame = new JFrame("Universo músical");
		
		JScrollPane scroll = new JScrollPane();
		scroll.setViewportView(jt);
		frame.add(scroll);


		frame.setSize(500, 200);
		frame.setVisible(true);
		
		
	}

}
