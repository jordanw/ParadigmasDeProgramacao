package paradigmas.exercicios.unidade2;

import java.util.List;

import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;

public class MusicaTableModel extends AbstractTableModel {

	List<Musica> musica;
	
	String[] colunas = {"Música","Artista", "Album", "Ano"};
	
	public MusicaTableModel (MusicaDAO dao) {
		//musica = dao.recupera();
		musica = dao.read();
	}
	
	public void addRow(Musica m)
	{
		this.musica.add(m);
		this.fireTableDataChanged();
	}
	
	public void removeRow(int linha)
	{
		this.musica.remove(linha);
		this.fireTableRowsDeleted(linha, linha);
	}
		
	public Musica get(int linha)
	{
		return this.musica.get(linha);
	}
	
	public int getRowCount() {
		// TODO Auto-generated method stub
		return musica.size();
	}

	
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return colunas.length;
	}

	
	
	public Object getValueAt(int rowIndex, int columnIndex) {
		// TODO Auto-generated method stub
		Musica c = musica.get(rowIndex);
		switch (columnIndex) {
			case 0: return c.getNome();
			case 1: return c.getArtista();
			case 2: return c.getAlbum();
			case 3: return c.getAno();
		}
		return null;
	}

	
	@Override
	public String getColumnName(int column) {
		// TODO Auto-generated method stub
		return colunas[column];
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		// TODO Auto-generated method stub
		Musica c = musica.get(rowIndex);
		switch (columnIndex) {
			case 0:
				c.setNome((String)aValue);
				break;
			case 1:
				c.setArtista((String)aValue);
				break;
			case 2:
				c.setAlbum((String)aValue);
				break;
			case 3:
				c.setAno((String)aValue);
				break;	
		}
		fireTableCellUpdated(rowIndex,columnIndex);
	}

}
