package paradigmas.serializacao;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

public class Salvando {
	public static void main(String[] args) {
		Pessoa p = new Pessoa("Jordan", "05563751309", 23, 1.74);
		try {
			
			OutputStream f2 = new FileOutputStream("saida.txt");
			OutputStreamWriter out = new OutputStreamWriter(f2); 
			BufferedWriter w = new BufferedWriter(out);
			ObjectOutputStream saida = new ObjectOutputStream(f2);
			
			saida.writeObject(p);
			
			FileWriter f = new FileWriter("salvando.txt");
			PrintWriter in  = new PrintWriter(f);
			//f.write(76);
			in.println(p.getNome());
			in.close();
			f.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
