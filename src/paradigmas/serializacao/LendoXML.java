package paradigmas.serializacao;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;

public class LendoXML {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
try {
			
			InputStream f = new FileInputStream("saida.txt");  
			//ObjectInputStream entrada = new ObjectInputStream(f);
			//Object o = (Pessoa) entrada.readObject();
			//System.out.println(((Pessoa) o).getNome());
			XMLDecoder decoder = new XMLDecoder(f);
			Pessoa p = (Pessoa)decoder.readObject();
			decoder.close();
			
			//entrada.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


	}

}
