package paradigmas.serializacao;

import java.beans.XMLEncoder;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

public class Lendo {

	public static void main(String[] args) throws ClassNotFoundException 
	{		
		Pessoa p = new Pessoa("Jordan", "05563751309", 23, 1.74);
		try {
			
			InputStream f = new FileInputStream("saida.txt");
			FileInputStream f2 = new FileInputStream("saida.txt");
			ObjectInputStream entrada = new ObjectInputStream(f);
			Object o = (Pessoa) entrada.readObject();
			System.out.println(((Pessoa) o).getNome());
			//XMLEncoder encoder = new XMLEncoder(entrada);
			entrada.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}

