package paradigmas.atividades01;

public class ContaCorrenteEspecial extends Conta
{
	private String nome;
    private int conta, saques;
    private double saldo;
	
    
	ContaCorrenteEspecial(String nome, int conta, double saldo_inicial)
	{
		super(nome, conta, saldo_inicial);
	}
	
	public void sacar(double valor){
        if(saldo >= valor){
            saldo -= (valor + valor*0.001);
            saques++;
            System.out.println("Sacado: " + valor);
            System.out.println("Novo saldo: " + saldo + "\n");
        } else {
            System.out.println("Saldo insuficiente. Faça um depósito\n");
        }
    }
    

}
