package paradigmas.atividades02;

public class DVD extends Loja 
{

	private String produto;
	private double duracao;
	private double preco;
	private int quantidade;
	private int codigoBarras;
	
	DVD(String produto,double duracao, double preco, int quantidade, int codigoBarras)
	{
		super(produto, preco, quantidade, codigoBarras);
		this.duracao = duracao;
	}
	
	public String toString() 
	{
		return "Produto: " + this.produto + 
				"\nDuração:"+ this.duracao + 
				"\n Preço: " + this.preco +
			    "\n Quantidade: " + this.quantidade + 
			    "\n Valor total: " + this.preco * this.quantidade;
	}
}
