package paradigmas.atividades02;

public class CD extends Loja 
{
	private String produto;
	private int numfaixas;
	private double preco;
	private int quantidade;
	private int codigoBarras;
	
	CD(String produto, int numFaixas, double preco, int quantidade, int codigoBarras)
	{
		super(produto, preco, quantidade, codigoBarras);
		this.numfaixas = numFaixas;
	}
	
	public String toString() 
	{
		return "Produto: " + this.produto + 
				"\nNúmero de faixas:"+ this.numfaixas + 
				"\n Preço: " + this.preco +
			    "\n Quantidade: " + this.quantidade + 
			    "\n Valor total: " + this.preco * this.quantidade;
	}
	
}
