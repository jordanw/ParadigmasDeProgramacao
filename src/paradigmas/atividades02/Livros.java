package paradigmas.atividades02;

public class Livros extends Loja 
{
	private String produto;
	private String autor;
	private double preco;
	private int quantidade;
	private int codigoBarras;
    
	Livros(String produto, String autor, double preco, int quantidade, int codigoBarras)
	{
		super(produto, preco, quantidade, codigoBarras);
		this.autor = autor;		
	}
	
	public String toString() 
	{
		return "Produto: " + this.produto + 
				"\nAuto:"+ this.autor + 
				"\n Preço: " + this.preco +
			    "\n Quantidade: " + this.quantidade + 
			    "\n Valor total: " + this.preco * this.quantidade;
	}
}
