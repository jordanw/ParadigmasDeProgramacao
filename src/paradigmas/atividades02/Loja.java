package paradigmas.atividades02;

public class Loja 
{
	private String produto;
	private double preco;
	private int quantidade;
	private int codigoBarras;
	
	Loja(String produto, double preco, int quantidade, int codigoBarras)
	{
		this.produto = produto;
		this.preco = preco;
		this.quantidade = quantidade;
		this.codigoBarras = codigoBarras;
		
	}
	
	
	public double valorTotal()
	{
		return this.preco * this.quantidade;
	}
	
	public void carrinho()
	{
		System.out.println("Produto: " + this.produto);
		System.out.println("Preço unitário: " + this.preco);
		System.out.println("Quantidade: " + this.quantidade);
		System.out.println("Total a pagar: " + this.valorTotal());
	}
	

	public String toString() {
		return "Produto: " + this.produto + 
				"\n Preço: " + this.preco +
			    "\n Quantidade: " + this.quantidade + 
			    "\n Valor total: " + this.preco * this.quantidade;
	}
	
	public boolean equals(Loja l1, Loja l2)
	{
			if(l1.codigoBarras == l2.codigoBarras)
			{
				System.out.println("Produtos iguais");
				return true;
			}
			return false;
	}

	public String getProduto() {
		return produto;
	}


	public void setProduto(String produto) {
		this.produto = produto;
	}


	public double getPreco() {
		return preco;
	}


	public void setPreco(double preco) {
		this.preco = preco;
	}


	public int getQuantidade() {
		return quantidade;
	}


	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}
	
}
